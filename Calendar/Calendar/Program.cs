﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calendar
{
    class Program
    {
        static int ThuCuaNgay(int Day, int Month, int Year)
        {
            int Thu;
            if (Month < 3)
            {
                Month += 12;
                Year--;
            }
            Thu = Math.Abs(Day + 2*Month + 3*(Month + 1)/5 + Year + Year/4) % 7;
            return Thu;
        }
        static void Main(string[] args)
        {
            string[,] Calendar = new string[7, 7];
            int Month, Year, ThuCuaNgayDauTien, Day = 1;
            char[] a = {' ', ' '};
            

            Calendar[0, 0] = "Su";
            Calendar[0, 1] = "Mo";
            Calendar[0, 2] = "Tu";
            Calendar[0, 3] = "We";
            Calendar[0, 4] = "Th";
            Calendar[0, 5] = "Fr";
            Calendar[0, 6] = "Sa";
            
            Console.WriteLine(" Nhap Month, Year: ");
            Month = Int32.Parse(Console.ReadLine());
            Year = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            ThuCuaNgayDauTien = ThuCuaNgay(1, Month, Year);

            //for (int i = ThuCuaNgayDauTien; i < 7; i++)
            //{
            //    if (Day < 10)
            //        Calendar[1, i] = " " + Day.ToString();
            //    else
            //        Calendar[1, i] = Day.ToString();
            //    Day++;
            //}

            //for (int i = 2; i < 7; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //    {
            //        if (Day < 10)
            //            Calendar[i, j] = " " + Day.ToString();
            //        else
            //            Calendar[i, j] = Day.ToString();

            //        Day++;
            //        if (Day > DateTime.DaysInMonth(Year, Month))
            //            break;
            //    }
            //    if (Day > DateTime.DaysInMonth(Year, Month))
            //        break;
            //}
            
            for (int i = 1; i < 7; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 1 && j < 7 - ThuCuaNgayDauTien)
                    {
                        if (Day < 10)
                            Calendar[i, j + ThuCuaNgayDauTien] = " " + Day.ToString();
                        else
                            Calendar[i, j + ThuCuaNgayDauTien] = Day.ToString();
                        if (j + ThuCuaNgayDauTien == 6)
                        {
                            Day++;
                            break;
                        }
                    }
                    else
                    {
                        if (Day < 10)
                            Calendar[i, j] = " " + Day.ToString();
                        else
                            Calendar[i, j] = Day.ToString();
                    }
                    Day++;
                    if (Day > DateTime.DaysInMonth(Year, Month))
                        break;
                }
                if (Day > DateTime.DaysInMonth(Year, Month))
                    break;
            }

            //Console.WriteLine(" Calendar[{0}, {1}] = {2}", 0, ThuCuaNgayDauTien, Calendar[0, ThuCuaNgayDauTien]);

            //for (int i = 0; i < 7; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //        if (Calendar[i, j] == null)
            //            Calendar[i, j] = "  ";
            //}

            //for (int i = 0; i < 7; i++)
            //    Console.Write(Calendar[0, i] + "  ");
            //Console.WriteLine();

            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (Calendar[i, j] == null)
                        Calendar[i, j] = "  ";
                    Console.Write(Calendar[i, j] + "  ");
                }
                Console.WriteLine();
            }
            
            Console.ReadLine();
        }
    }
}