#ifndef _GACH_header
#define _GACH_header

#include "Public.h"

#define SoLuongGachToiDa	10


struct Gach
{
	ToaDo toaDo;
	int w, h;
	char hinhDang[3][3];
};


struct QuanLyGach
{
	Gach a[SoLuongGachToiDa];
	int n;
};


void QLG_KhoiTaoQuanLy(QuanLyGach &qlg);
void QLG_TaoHinhDang_trenBuffer(QuanLyGach qlg, Buffer &buffer);

void QLG_TaoGach(QuanLyGach &qlg, int idGach, ToaDo toadoGach);
void QLG_XoaGach(QuanLyGach &qlg, int vitriXoa);

void QLG_QuanLy(QuanLyGach &qlg, int bufferH);


#endif