﻿#ifndef _XE_TANG_header
#define _XE_TANG_header

#include "Dan.h"


struct XeTang
{
	ToaDo toaDo;

	int hp;			// máu
	QuanLyDan qlDan;	// quản lý đạn


	// _hd là mảng 2 chiều, kích cỡ 3x3 lưu trữ hình dạng của xe tăng
	// HinhDang <=> _hd
	// sự khác biệt: HinhDang "được đặt ngay vị trí trung tâm của xe tăng"
	//
	//		_hd[0][0] <=> HinhDang[-1][-1]
	//		_hd[0][1] <=> HinhDang[-1][0]
	//		_hd[0][2] <=> HinhDang[-1][1]
	//		...
	//		_hd[1][1] <=> HinhDang[0][0]

	char _hd[3][3];
	char (*hinhDang)[3];
};


void XeTang_KhoiTao(XeTang &xeTang, int bufferW, int bufferH);

void XeTang_TaoHinhDang_trenBuffer(XeTang xeTang, Buffer &buffer);
void XeTang_DiChuyen(XeTang &xeTang, HuongDiChuyen huongDiChuyen);

void XeTang_BanDan(XeTang &xeTang);
void XeTang_QuanLyDan(XeTang &xeTang);


#endif