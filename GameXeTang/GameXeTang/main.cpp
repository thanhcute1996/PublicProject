﻿/*
Game bắn xe tăng
Code demo cho các bạn nên chức năng rất đơn giản, code nhanh trong 1 buổi chiều + tối là xong
- Các chức năng:
	+ Điều khiển xe tăng (trái, phải, lên, xuống)
	+ Bắn đạn
	+ Gạch rơi xuống


*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include "XeTang.h"
#include "Gach.h"
#include "XuLy.h"
#include "console.h"


void KhoiTao(Buffer &buffer, XeTang &player, QuanLyGach &qlg);
void KhoiTaoManHinh(int bufferW, int bufferH);
void XoaBuffer(Buffer &buffer);
void InRaManHinh(Buffer &buffer, XeTang player, QuanLyGach qlg);




int main()
{
	Buffer buffer;
	XeTang player;
	QuanLyGach qlg;

	srand((unsigned)time(0));

	clrscr();	
	KhoiTao(buffer, player, qlg);
	KhoiTaoManHinh(buffer.w, buffer.h);

	while (1)
	{
		DieuKhien(player, buffer.w, buffer.h);
		int maXuLy = XuLy(player, qlg, buffer.w, buffer.h);
		InRaManHinh(buffer, player, qlg);

		Sleep(50);

		if (maXuLy == -1)
		{
			// game over
			gotoXY(buffer.w + 4, 10);
			printf("Game over !!! An ESC de thoat");

			while (_getch() != 27);
			break;
		}
	}

	return 0;
}



void KhoiTao(Buffer &buffer, XeTang &player, QuanLyGach &qlg)
{
	buffer.w = 30;
	buffer.h = 24;

	XeTang_KhoiTao(player, buffer.w, buffer.h);
	QLG_KhoiTaoQuanLy(qlg);
	
	XoaBuffer(buffer);
	XeTang_TaoHinhDang_trenBuffer(player, buffer);
}

void KhoiTaoManHinh(int bufferW, int bufferH)
{
	for (int i = 0; i < bufferH; i++)
	{
		gotoXY(bufferW, i);
		printf("@");
	}
}

void XoaBuffer(Buffer &buffer)
{
	for (int i = 0; i < buffer.h; i++)
		for (int j = 0; j < buffer.w; j++)
			buffer.a[i][j] = ' ';
}


void InRaManHinh(Buffer &buffer, XeTang player, QuanLyGach qlg)
{
	XoaBuffer(buffer);

	//------------------

	XeTang_TaoHinhDang_trenBuffer(player, buffer);
	QLG_TaoHinhDang_trenBuffer(qlg, buffer);

	//------------------

	gotoXY(0, 0);
	for (int i = 0; i < buffer.h; i++)
	{
		for (int j = 0; j < buffer.w; j++)
			printf("%c", buffer.a[i][j]);

		printf("\n");
	}


	gotoXY(buffer.w + 4, 5);
	printf("                  ");
	gotoXY(buffer.w + 4, 5);
	printf("HP: %d", player.hp);
}