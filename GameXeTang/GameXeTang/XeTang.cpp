﻿#include "XeTang.h"
#include "Public.h"

void XeTang_KhoiTao(XeTang &xeTang, int bufferW, int bufferH)
{
	xeTang.toaDo.x = bufferW / 2;
	xeTang.toaDo.y = bufferH - 2;
	xeTang.hp = 10;

	QLDan_KhoiTaoQuanLy(xeTang.qlDan);


	// quan trọng
	xeTang.hinhDang = (char(*)[3])((char*)&xeTang._hd[1] + 1);


	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++)
			xeTang.hinhDang[i][j] = ' ';

	xeTang.hinhDang[-1][0] = 'i';
	xeTang.hinhDang[0][-1] = 'o';
	xeTang.hinhDang[0][0] = 'O';
	xeTang.hinhDang[0][1] = 'o';
	xeTang.hinhDang[1][-1] = 'M';
	xeTang.hinhDang[1][1] = 'M';
}



void XeTang_TaoHinhDang_trenBuffer(XeTang xeTang, Buffer &buffer)
{
	// hình dạng xe tăng
	for (int kx = -1; kx <= 1; kx++)
		for (int ky = -1; ky <= 1; ky++)
		{
			int toadoX = xeTang.toaDo.x + kx;
			int toadoY = xeTang.toaDo.y + ky;

			// nếu tọa độ x hoặc tọa độ y vượt quá kích cỡ buffer thì bỏ qua
			if (toadoX >= buffer.w || toadoY >= buffer.h)
				continue;

			char kitu = xeTang.hinhDang[ky][kx];

			if (kitu != ' ')
			{
				buffer.a[toadoY][toadoX] = kitu;
			}
		}



	// hình dạng viên đạn
	for (int i = 0; i < xeTang.qlDan.n; i++)
	{
		Dan dan = xeTang.qlDan.a[i];
		ToaDo td = dan.toaDo;

		buffer.a[td.y][td.x] = dan.hinhDang;
	}
}


void XeTang_DiChuyen(XeTang &xeTang, HuongDiChuyen huongDiChuyen)
{
	if (huongDiChuyen == LenTren)
		xeTang.toaDo.y--;
	else if (huongDiChuyen == XuongDuoi)
		xeTang.toaDo.y++;
	else if (huongDiChuyen == QuaTrai)
		xeTang.toaDo.x--;
	else if (huongDiChuyen == QuaPhai)
		xeTang.toaDo.x++;
}


//---------------------------------------------------------

void XeTang_BanDan(XeTang &xeTang)
{
	ToaDo toadoBan = xeTang.toaDo;
	toadoBan.y--;

	QLDan_BanVienDan(xeTang.qlDan, toadoBan);
}


void XeTang_QuanLyDan(XeTang &xeTang)
{
	QLDan_QuanLy(xeTang.qlDan);
}
