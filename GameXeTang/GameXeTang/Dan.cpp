﻿#include "Dan.h"
#include "console.h"


// khởi tạo viên đạn
Dan Dan_TaoVienDan(ToaDo tdKhoiTao)
{
	Dan dan;
	dan.hinhDang = 30;
	dan.mauSac = ColorCode_Red;
	dan.toaDo = tdKhoiTao;

	return dan;
}

//-------------------------------------------------------

void QLDan_KhoiTaoQuanLy(QuanLyDan &qlDan)
{
	qlDan.n = 0;
}


void QLDan_BanVienDan(QuanLyDan &qlDan, ToaDo toadoBan)
{
	// không được vượt quá số lượng đạn tối đa
	if (qlDan.n >= SoLuongDanToiDa)
		return;

	int n = qlDan.n;
	qlDan.a[n] = Dan_TaoVienDan(toadoBan);

	// tăng số lượng đạn lên 1 đơn vị
	qlDan.n++;
}


// xóa 1 viên đạn khỏi danh sách
void QLDan_XoaVienDan(QuanLyDan &qlDan, int vitriXoa)
{
	qlDan.n--;

	for (int i = vitriXoa; i < qlDan.n; i++)
		qlDan.a[i] = qlDan.a[i + 1];
}



void QLDan_QuanLy(QuanLyDan &qlDan)
{
	for (int i = 0; i < qlDan.n; i++)
	{
		Dan dan = qlDan.a[i];

		if (dan.toaDo.y < 0)
		{
			QLDan_XoaVienDan(qlDan, i);
			i--;
		}
	}
}