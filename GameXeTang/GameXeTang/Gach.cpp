﻿#include "Gach.h"


Gach Gach_TaoVienGach(int id, ToaDo tdKhoiTao)
{
	Gach gach;
	gach.toaDo = tdKhoiTao;
	gach.w = gach.h = 0;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			gach.hinhDang[i][j] = ' ';

	char (*hd)[3] = gach.hinhDang;
	char kitu = 'x';

	if (id == 1)
	{
		gach.w = gach.h = 2;
		hd[0][0] = hd[0][1] = hd[1][0] = hd[1][1] = kitu;
	}
	else if (id == 2)
	{
		gach.w = 3;
		gach.h = 1;
		hd[0][0] = hd[0][1] = hd[0][2] = kitu;
	}
	else if (id == 3)
	{
		gach.w = 1;
		gach.h = 3;
		hd[0][0] = hd[1][0] = hd[2][0] = kitu;
	}
	else
	{
		gach.w = gach.h = 3;
		hd[0][0] = hd[1][0] = hd[2][0] = kitu;
		hd[2][1] = hd[2][2] = kitu;
	}

	return gach;
}

//----------------------------------------------------

void QLG_KhoiTaoQuanLy(QuanLyGach &qlg)
{
	qlg.n = 0;
}


void QLG_TaoHinhDang_trenBuffer(QuanLyGach qlg, Buffer &buffer)
{
	for (int igach = 0; igach < qlg.n; igach++)
	{
		Gach gach = qlg.a[igach];
		ToaDo td = gach.toaDo;
		
		for (int i = 0; i < gach.h; i++)
			for (int j = 0; j < gach.w; j++)
			{
				int toadoX = td.x + j;
				int toadoY = td.y + i;

				if (toadoX >= buffer.w || toadoY >= buffer.h || toadoX < 0 || toadoY < 0)
					continue;

				buffer.a[toadoY][toadoX] = gach.hinhDang[i][j];
			}
	}
}


void QLG_TaoGach(QuanLyGach &qlg, int idGach, ToaDo toadoGach)
{
	if (qlg.n >= SoLuongGachToiDa)
		return;

	int n = qlg.n;
	qlg.a[n] = Gach_TaoVienGach(idGach, toadoGach);

	// tăng số lượng gạch lên 1 đơn vị
	qlg.n++;
}


void QLG_XoaGach(QuanLyGach &qlg, int vitriXoa)
{
	qlg.n--;

	for (int i = vitriXoa; i < qlg.n; i++)
		qlg.a[i] = qlg.a[i + 1];
}


void QLG_QuanLy(QuanLyGach &qlg, int bufferH)
{
	for (int i = 0; i < qlg.n; i++)
	{
		Gach gach = qlg.a[i];

		if (gach.toaDo.y >= bufferH)
		{
			QLG_XoaGach(qlg, i);
			i--;
		}
	}
}