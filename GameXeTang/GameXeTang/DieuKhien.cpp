#include <Windows.h>
#include <conio.h>
#include "XuLy.h"


void DieuKhien(XeTang &player, int bufferW, int bufferH)
{
	int keyLeft = GetAsyncKeyState('A') || GetAsyncKeyState(VK_LEFT);
	int keyRight = GetAsyncKeyState('D') || GetAsyncKeyState(VK_RIGHT);
	int keyUp = GetAsyncKeyState('W') || GetAsyncKeyState(VK_UP);
	int keyDown = GetAsyncKeyState('S') || GetAsyncKeyState(VK_DOWN);
	int keySpace = GetAsyncKeyState(VK_SPACE);


	if (keyLeft)
	{
		if (player.toaDo.x > 1)
			XeTang_DiChuyen(player, QuaTrai);
	}
	else if (keyRight)
	{
		if (player.toaDo.x < bufferW - 2)
			XeTang_DiChuyen(player, QuaPhai);
	}
	else if (keyUp)
	{
		if (player.toaDo.y > 1)
			XeTang_DiChuyen(player, LenTren);
	}
	else if (keyDown)
	{
		if (player.toaDo.y < bufferH - 2)
			XeTang_DiChuyen(player, XuongDuoi);
	}

	if (keySpace)
		XeTang_BanDan(player);
}