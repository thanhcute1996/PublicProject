﻿#ifndef _DAN_header
#define _DAN_header

#include "Public.h"

#define SoLuongDanToiDa	3


struct Dan
{
	ToaDo toaDo;
	char hinhDang;
	int mauSac;		// màu sắc
};


struct QuanLyDan
{
	Dan a[SoLuongDanToiDa];	// danh sách đạn

	// số lượng viên đạn đang được bắn
	// n = 0: xe tăng chưa bắn đạn
	// n = 1: xe tăng bắn 1 viên đạn
	// n = 3: ....
	int n;
};


void QLDan_KhoiTaoQuanLy(QuanLyDan &qlDan);
void QLDan_BanVienDan(QuanLyDan &qlDan, ToaDo toadoBan);
void QLDan_QuanLy(QuanLyDan &qlDan);


#endif