﻿#include "XuLy.h"


int KiemTraVaCham(ToaDo toado, Gach gach)
{
	for (int i = 0; i < gach.h; i++)
		for (int j = 0; j < gach.w; j++)
		{
			int tdGach_X = gach.toaDo.x + j;
			int tdGach_Y = gach.toaDo.y + i;

			if (toado.x == tdGach_X && toado.y == tdGach_Y)
				return 1;
		}

	return 0;
}


int KiemTraVaCham(XeTang player, Gach gach)
{
	// hình dạng xe tăng
	for (int kx = -1; kx <= 1; kx++)
		for (int ky = -1; ky <= 1; ky++)
		{
			int toadoX = player.toaDo.x + kx;
			int toadoY = player.toaDo.y + ky;
			ToaDo td = { toadoX, toadoY };

			char kitu = player.hinhDang[ky][kx];

			if (kitu != ' ')
			{
				int vacham = KiemTraVaCham(td, gach);

				if (vacham)
					return 1;
			}
		}

	return 0;
}


void XuLyVaCham_Dan_Gach(XeTang &player, QuanLyGach &qlg)
{
	for (int i = 0; i < player.qlDan.n; i++)
		for (int j = 0; j < qlg.n; j++)
		{
			int vacham = KiemTraVaCham(player.qlDan.a[i].toaDo, qlg.a[j]);

			if (vacham)
			{
				QLG_XoaGach(qlg, j);
				j--;
			}
		}
}


void XuLyVaCham_Player_Gach(XeTang &player, QuanLyGach &qlg)
{
	for (int i = 0; i < qlg.n; i++)
	{
		Gach gach = qlg.a[i];
		int vacham = KiemTraVaCham(player, gach);

		if (vacham)
		{
			QLG_XoaGach(qlg, i);
			i--;

			player.hp--;
		}
	}
}


void XuLyVaCham(XeTang &player, QuanLyGach &qlg)
{
	XuLyVaCham_Dan_Gach(player, qlg);
	XuLyVaCham_Player_Gach(player, qlg);
}
