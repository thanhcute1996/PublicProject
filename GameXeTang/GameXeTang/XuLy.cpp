#include <stdlib.h>
#include "XuLy.h"

void XuLyVaCham(XeTang &player, QuanLyGach &qlg);
void XuLyVaCham_Dan_Gach(XeTang &player, QuanLyGach &qlg);


int XuLy(XeTang &player, QuanLyGach &qlg, int bufferW, int bufferH)
{
	for (int i = 0; i < qlg.n; i++)
		qlg.a[i].toaDo.y++;

	QLG_QuanLy(qlg, bufferH);


	int k_taoGach = rand() % 20;
	if (k_taoGach >= 1 && k_taoGach <= 4)
	{
		ToaDo td;
		td.x = rand() % bufferW;
		td.y = -2;

		QLG_TaoGach(qlg, k_taoGach, td);
	}


	XuLyVaCham(player, qlg);


	for (int i = 0; i < player.qlDan.n; i++)
		player.qlDan.a[i].toaDo.y--;

	XeTang_QuanLyDan(player);

	XuLyVaCham_Dan_Gach(player, qlg);


	if (player.hp < 0)
		return -1;

	return 0;
}