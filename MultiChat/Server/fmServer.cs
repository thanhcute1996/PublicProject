﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Client;

namespace Server
{
    public partial class fmServer : Form
    {
        private int Id = 0; // Id là số thứ tự đăng nhập vào.

        private IPEndPoint IP;

        private Socket Server;

        private List<Socket> ClientList;

        public fmServer()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            Connect();
        }

        /// <summary>
        /// Kết nối với Server
        /// </summary>
        private void Connect()
        {
            // IP server
            IP = new IPEndPoint(IPAddress.Any, 9999);
            Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            ClientList = new List<Socket>();

            Server.Bind(IP);

            Thread Listen = new Thread(() => 
            {
                try
                {
                    while (true)
                    {
                        Server.Listen(100);
                        Socket Client = Server.Accept();
                        
                        Client.SetId(Id);
                        Id++;
                        ClientList.Add(Client);
                        
                        Thread Rec = new Thread(Receive);
                        Rec.IsBackground = true;
                        Rec.Start(Client);
                    }
                }
                catch
                {
                    IP = new IPEndPoint(IPAddress.Any, 9999);
                    Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }
            });
            Listen.IsBackground = true;
            Listen.Start();
        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        private void Close()
        {
            Server.Close();
        }

        /// <summary>
        /// Gửi tin từ Client
        /// </summary>
        private void Send(Socket Client)
        {
            if (Client != null && txtContent.Text != null)
                Client.Send(Serialize(txtContent.Text));
        }

        /// <summary>
        /// Nhận tin của Server
        /// </summary>
        private void Receive(object obj)
        {
            Socket Client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    Client.Receive(data);
                    string message = (string)Deserialize(data);
                    
                    foreach (Socket item in ClientList)
                    {
                        if (item != null && item != Client) 
                            item.Send(Serialize(message));                        
                    }
                    AddMessage(message);
                }
            }
            catch
            {
                ClientList.Remove(Client);
                Client.Close();
            }

        }

        /// <summary>
        /// Thêm Message vào khung chat
        /// </summary>
        /// <param name="message"></param>
        private void AddMessage(string message)
        {
            if (message.IndexOf("đã online") == -1 && message.IndexOf("đang trả lời...") == -1)
                lsvMessage.Items.Add(new ListViewItem() { Text = message });
            else
                lsvMessage.Items.Add(new ListViewItem() { Text = message, Font = new Font("Arial", 10, FontStyle.Italic) });
        }

        /// <summary>
        /// Phân mảnh dữ liệu truyền vào
        /// </summary>
        /// <param name="obj">Dữ liệu cần phân mảnh (kiểu string)</param>
        /// <returns>Return mảng byte đã phân mảnh</returns>
        private byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, obj);

            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh dữ liệu lại
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();

            return formatter.Deserialize(stream);
        }

        /// <summary>
        /// Đóng Form sẽ đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fmServer_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Gửi tin cho tất cả Client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
            foreach (Socket item in ClientList)
            {
                Send(item);
            }
            AddMessage(txtContent.Text);
            txtContent.Clear();
            
        }
    }
}
