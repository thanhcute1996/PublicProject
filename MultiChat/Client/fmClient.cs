﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Client
{
    public partial class fmClient : Form
    {
        private bool IsClickSend = true;

        private bool IsShowing = true;

        public delegate void ActionForm();

        public ActionForm ShowForm1;

        public ActionForm ClearTextBox;

        private User LoginUser;

        private IPEndPoint IP;

        private Socket Client;

        public fmClient(User login)
        {
            InitializeComponent();

            if (login != null)
            {
                LoginUser = login;
            }
            else
            {
                LoginUser = new User();
                btnDangXuat.Enabled = false; // Người lạ thì không có chế độ đăng xuất
            }
            
            // Lấy tên đăng nhập
            Binding data = new Binding("Text", LoginUser, "Id");
            txtID.DataBindings.Add(data);

            CheckForIllegalCrossThreadCalls = false;
            Connect();
        }

        /// <summary>
        /// Kết nối với Server
        /// </summary>
        private void Connect()
        { 
            // IP server
            IP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9999);
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                Client.Connect(IP);
            }
            catch
            {
                MessageBox.Show(" Không thể kết nối với server !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }
            
            Thread listen = new Thread(Receive);
            listen.IsBackground = true;
            listen.Start();
            Client.Send(Serialize(LoginUser.Id + " đã online"));
            lsvMessage.Items.Add(new ListViewItem() { Text = LoginUser.Id + " đã online", Font = new Font("Arial", 12, FontStyle.Italic) });
        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        private void CloseForm()
        {
            Client.Close();
        }

        /// <summary>
        /// Gửi tin từ Client
        /// </summary>
        private void Send()
        {
            if (txtContent.Text != null)
                Client.Send(Serialize(txtContent.Text));
        }

        /// <summary>
        /// Nhận tin của Server
        /// </summary>
        private void Receive()
        {
            try
            { 
                while(true)
                {
                    byte[] data = new byte[1024 * 5000];
                    Client.Receive(data);
                    string message = (string)Deserialize(data);
                    AddMessage(message);   
                }
            }
            catch
            {
                CloseForm();
            }
            
        }

        /// <summary>
        /// Thêm Message vào khung chat
        /// </summary>
        /// <param name="message"></param>
        private void AddMessage(string message)
        {
            if (message.IndexOf("đã online") == -1 && message.IndexOf("đang trả lời...") == -1)
                lsvMessage.Items.Add(new ListViewItem() { Text = message });
            else
                lsvMessage.Items.Add(new ListViewItem() { Text = message, Font = new Font("Arial", 10, FontStyle.Italic) });

            txtContent.Clear();
        }

        /// <summary>
        /// Phân mảnh dữ liệu truyền vào
        /// </summary>
        /// <param name="obj">Dữ liệu cần phân mảnh (kiểu string)</param>
        /// <returns>Return mảng byte đã phân mảnh</returns>
        private byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, obj);

            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh dữ liệu lại
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();

            return formatter.Deserialize(stream);
        }

        /// <summary>
        /// Event Click Button để gửi tin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
            Send();
            AddMessage(txtContent.Text);
            IsClickSend = true;
            
        }

        /// <summary>
        /// Đóng Form thì sẽ đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            if (IsShowing)
                ShowForm1();
            IsShowing = false;
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            IsShowing = true;
            ClearTextBox();
            CloseForm();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            CloseForm();
            IsShowing = false;
            Application.Exit();
        }

        private void txtContent_TextChanged(object sender, EventArgs e)
        {
            if (txtContent.Text != null && IsClickSend)
            {
                Client.Send(Serialize(LoginUser.Id + " đang trả lời..."));
                IsClickSend = false;
            }
        }
    }
}
