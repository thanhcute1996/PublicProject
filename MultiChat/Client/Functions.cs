﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Client
{
    public static class Functions
    {
        public static int Id;

        public static int GetId(this Socket Client)
        {
            return Id;    
        }

        public static void SetId(this Socket Client, int id)
        {
            Id = id;
        }
    }
}
