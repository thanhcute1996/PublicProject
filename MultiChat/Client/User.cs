﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Client
{
    public class User
    {
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public User()
        {
            id = "Người Lạ";
        }
    }
}
