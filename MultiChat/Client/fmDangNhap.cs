﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public partial class fmDangNhap : Form
    {
        private User LoginUser;
        private List<User> ListUser;

        public fmDangNhap()
        {
            InitializeComponent();
            InfoLogin();
        }

        /// <summary>
        /// Thông tin tài khoản đăng nhập
        /// </summary>
        private void InfoLogin()
        {
            ListUser = new List<User>();

            ListUser.Add(new User() { Id = "Vi Thanh", Password = "axinentroy" });
            ListUser.Add(new User() { Id = "Lê Tiệp", Password = "levanAnh" });
            ListUser.Add(new User() { Id = "Vô Tâm", Password = "ducthu719" });
            ListUser.Add(new User() { Id = "Thương Bé", Password = "thuong196" });
        }

        /// <summary>
        /// Show Form 1
        /// </summary>
        private void ShowForm()
        {
            this.Show();
        }

        private void ClearTextBox()
        {
            foreach (var item in this.Controls)
            {
                TextBox tmp = item as TextBox;

                if (tmp != null)
                    tmp.Clear();
            }
            txtID.Focus();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            foreach (User item in ListUser)
	        {
                if (txtID.Text == item.Id && txtPassword.Text == item.Password)
                {
                    LoginUser = item;
                    MessageBox.Show("Đăng nhập thành công !", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    break;
                }
	        }

            if (LoginUser == null)
            {
                MessageBox.Show("Bạn nhập sai tài khoản hoặc sai mật khẩu !\n Vui lòng đăng nhập lại !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtID.Focus();
            }
            else
            {
                fmClient Client = new fmClient(LoginUser);
                Client.ShowForm1 = new fmClient.ActionForm(ShowForm);
                Client.ClearTextBox = new fmClient.ActionForm(ClearTextBox);
                Client.Show();
                this.Hide();
            }
            
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNguoiLa_Click(object sender, EventArgs e)
        {
            fmClient Client = new fmClient(null);
            Client.ShowForm1 = new fmClient.ActionForm(ShowForm);
            Client.Show();
            this.Hide();
        }
    }
}
