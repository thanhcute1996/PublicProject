﻿namespace SmartSideBarPersonal
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lbName = new System.Windows.Forms.Label();
            this.picbAvartar = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbPath = new System.Windows.Forms.Label();
            this.btnChooseImage = new System.Windows.Forms.Button();
            this.btnChooseFont = new System.Windows.Forms.Button();
            this.ckbOutLine = new System.Windows.Forms.CheckBox();
            this.txtNumberOutLine = new System.Windows.Forms.TextBox();
            this.ckbStartUp = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.openPathDialog = new System.Windows.Forms.OpenFileDialog();
            this.FontDialog = new System.Windows.Forms.FontDialog();
            this.tltNote = new System.Windows.Forms.ToolTip(this.components);
            this.btnChooseColor = new System.Windows.Forms.Button();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.picbAvartar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(10, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên hiển thị";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.txtName.Location = new System.Drawing.Point(110, 22);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(216, 26);
            this.txtName.TabIndex = 1;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            this.txtName.Enter += new System.EventHandler(this.txtName_Enter);
            this.txtName.Leave += new System.EventHandler(this.txtName_Leave);
            // 
            // lbName
            // 
            this.lbName.Font = new System.Drawing.Font("Segoe UI", 20.75F);
            this.lbName.Location = new System.Drawing.Point(19, 250);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(325, 46);
            this.lbName.TabIndex = 3;
            this.lbName.Text = "NAME";
            this.lbName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picbAvartar
            // 
            this.picbAvartar.BackColor = System.Drawing.Color.Transparent;
            this.picbAvartar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picbAvartar.Location = new System.Drawing.Point(98, 78);
            this.picbAvartar.Name = "picbAvartar";
            this.picbAvartar.Size = new System.Drawing.Size(170, 170);
            this.picbAvartar.TabIndex = 2;
            this.picbAvartar.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label2.Location = new System.Drawing.Point(14, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Đường dẫn";
            // 
            // lbPath
            // 
            this.lbPath.Location = new System.Drawing.Point(120, 297);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(206, 57);
            this.lbPath.TabIndex = 5;
            // 
            // btnChooseImage
            // 
            this.btnChooseImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnChooseImage.Location = new System.Drawing.Point(118, 355);
            this.btnChooseImage.Name = "btnChooseImage";
            this.btnChooseImage.Size = new System.Drawing.Size(110, 32);
            this.btnChooseImage.TabIndex = 6;
            this.btnChooseImage.Text = "Chọn hình ảnh";
            this.btnChooseImage.UseVisualStyleBackColor = true;
            this.btnChooseImage.Click += new System.EventHandler(this.btnChooseImage_Click);
            // 
            // btnChooseFont
            // 
            this.btnChooseFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnChooseFont.Location = new System.Drawing.Point(239, 355);
            this.btnChooseFont.Name = "btnChooseFont";
            this.btnChooseFont.Size = new System.Drawing.Size(87, 32);
            this.btnChooseFont.TabIndex = 7;
            this.btnChooseFont.Text = "Chọn Font";
            this.btnChooseFont.UseVisualStyleBackColor = true;
            this.btnChooseFont.Click += new System.EventHandler(this.btnChooseFont_Click);
            // 
            // ckbOutLine
            // 
            this.ckbOutLine.AutoSize = true;
            this.ckbOutLine.Checked = true;
            this.ckbOutLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbOutLine.Location = new System.Drawing.Point(13, 402);
            this.ckbOutLine.Name = "ckbOutLine";
            this.ckbOutLine.Size = new System.Drawing.Size(47, 17);
            this.ckbOutLine.TabIndex = 8;
            this.ckbOutLine.Text = "Viền";
            this.ckbOutLine.UseVisualStyleBackColor = true;
            this.ckbOutLine.CheckedChanged += new System.EventHandler(this.ckbOutLine_CheckedChanged);
            // 
            // txtNumberOutLine
            // 
            this.txtNumberOutLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.txtNumberOutLine.Location = new System.Drawing.Point(66, 397);
            this.txtNumberOutLine.Name = "txtNumberOutLine";
            this.txtNumberOutLine.Size = new System.Drawing.Size(100, 23);
            this.txtNumberOutLine.TabIndex = 9;
            this.txtNumberOutLine.TextChanged += new System.EventHandler(this.txtNumberOutLine_TextChanged);
            this.txtNumberOutLine.Enter += new System.EventHandler(this.txtNumberOutLine_Enter);
            this.txtNumberOutLine.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumberOutLine_KeyPress);
            this.txtNumberOutLine.Leave += new System.EventHandler(this.txtNumberOutLine_Leave);
            // 
            // ckbStartUp
            // 
            this.ckbStartUp.AutoSize = true;
            this.ckbStartUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.ckbStartUp.Location = new System.Drawing.Point(172, 398);
            this.ckbStartUp.Name = "ckbStartUp";
            this.ckbStartUp.Size = new System.Drawing.Size(186, 21);
            this.ckbStartUp.TabIndex = 10;
            this.ckbStartUp.Text = "Khởi động cùng hệ thống";
            this.ckbStartUp.UseVisualStyleBackColor = true;
            this.ckbStartUp.CheckedChanged += new System.EventHandler(this.ckbStartUp_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnSave.Location = new System.Drawing.Point(172, 447);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnCancel.Location = new System.Drawing.Point(265, 447);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label4.Location = new System.Drawing.Point(13, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Xem trước:";
            // 
            // openPathDialog
            // 
            this.openPathDialog.Title = "Chọn ảnh Avartar";
            // 
            // tltNote
            // 
            this.tltNote.ToolTipTitle = "Note:";
            // 
            // btnChooseColor
            // 
            this.btnChooseColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnChooseColor.Location = new System.Drawing.Point(18, 355);
            this.btnChooseColor.Name = "btnChooseColor";
            this.btnChooseColor.Size = new System.Drawing.Size(87, 32);
            this.btnChooseColor.TabIndex = 14;
            this.btnChooseColor.Text = "Chọn màu";
            this.btnChooseColor.UseVisualStyleBackColor = true;
            this.btnChooseColor.Click += new System.EventHandler(this.btnChooseColor_Click);
            // 
            // SettingForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(364, 488);
            this.Controls.Add(this.btnChooseColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ckbStartUp);
            this.Controls.Add(this.txtNumberOutLine);
            this.Controls.Add(this.ckbOutLine);
            this.Controls.Add(this.btnChooseFont);
            this.Controls.Add(this.btnChooseImage);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.picbAvartar);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingForm";
            this.Text = "Thiết lập thông tin";
            this.Load += new System.EventHandler(this.SettingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picbAvartar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.PictureBox picbAvartar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.Button btnChooseImage;
        private System.Windows.Forms.Button btnChooseFont;
        private System.Windows.Forms.CheckBox ckbOutLine;
        private System.Windows.Forms.TextBox txtNumberOutLine;
        private System.Windows.Forms.CheckBox ckbStartUp;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog openPathDialog;
        private System.Windows.Forms.FontDialog FontDialog;
        private System.Windows.Forms.ToolTip tltNote;
        private System.Windows.Forms.Button btnChooseColor;
        private System.Windows.Forms.ColorDialog ColorDialog;
    }
}