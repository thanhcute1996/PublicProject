﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using System.Drawing.Imaging;

namespace SmartSideBarPersonal
{
    public partial class SettingForm : Form
    {
        static SettingForm SF;
        static DialogResult DR = DialogResult.Cancel;

        private bool IsChangedPicture = false;
        private string ImgRoot;
        private bool IsChangedInfo = false;

        public SettingForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Đặt lại giá trị  cho các thuộc tính.
        /// </summary>
        private void SetProperty()
        {
            try
            {
                // Load Image
                Image img = Image.FromStream(new MemoryStream(File.ReadAllBytes(Application.StartupPath + "\\avt.dat")));
                picbAvartar.Image = img;

                // Load text
                string[] body = File.ReadAllLines(Application.StartupPath + "\\name.dat");
                lbName.Text = body[0];
                lbName.Font = new System.Drawing.Font(new FontFamily(body[1]), float.Parse(body[2]), body[3] == "Bold" ?
                    FontStyle.Bold : (body[3] == "Italic" ? FontStyle.Italic : System.Drawing.FontStyle.Regular));
                txtName.Text = body[0];

                // Load Path
                lbPath.Text = Application.StartupPath + "\\avt.dat";

                // Load Color
                //lbName.ForeColor = Color.FromName(body[6]);
                string[] TextColor = body[6].Split(' ');
                lbName.ForeColor = Color.FromArgb(Int32.Parse(TextColor[0]), Int32.Parse(TextColor[1]), Int32.Parse(TextColor[2]), Int32.Parse(TextColor[3]));
                
                ckbStartUp.Checked = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", true).GetValue("SmartSideBarPersonal") == null ? false : true;

                // Load Viền
                txtNumberOutLine.Text = body[5];
                ckbOutLine.CheckState = ((body[4].Equals("True")) ? CheckState.Checked : CheckState.Unchecked);
            }
            catch 
            { 
            
            }
        }

        /// <summary>
        /// Kiểm tra dữ liệu đầu vào của textbox Name.
        /// </summary>
        /// <returns>Trả về true (Dữ liệu hợp lệ) khi số ký tự không quá 20 ký tự, ngược lại trả vè false</returns>
        private bool ValidateName()
        {
            if (txtName.Text.Length <= 20)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Kiểm tra dữ liệu của textbox NumberOutline.
        /// </summary>
        /// <returns>trả về true (hợp lệ) khi giá trị không quá 50, ngược lại trả về false.</returns>
        private bool ValidateNumberOutline()
        {
            if (Int32.Parse(txtNumberOutLine.Text.Trim()) <= 100)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Crop lại hình ảnh theo hình tròn.
        /// </summary>
        /// <param name="ImagePath">Đường dẫn hình ảnh</param>
        /// <param name="CircleSize">Đường kính hình tròn</param>
        /// <returns></returns>
        private Image ImageCrop(string ImagePath, int CircleSize)
        {
            int CircleDiameter; // Đường kính hình tròn

            Bitmap bm = new Bitmap(Image.FromStream(new MemoryStream(File.ReadAllBytes(ImagePath))));
            if (bm.Width > bm.Height)
            {
                CircleDiameter = bm.Height;
            }
            else
                CircleDiameter = bm.Width;

            Rectangle CropRect = new Rectangle(0, 0, CircleDiameter, CircleDiameter);
            bm = bm.Clone(CropRect, bm.PixelFormat);

            TextureBrush tb = new TextureBrush(bm);
            Bitmap final = new Bitmap(CircleDiameter, CircleDiameter);
            Graphics gp = Graphics.FromImage(final);

            // Bo góc ảnh
            gp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            // Tô màu cho elip với màu là màu của cọ tb
            gp.FillEllipse(tb, 0, 0, CircleDiameter, CircleDiameter);

            // Vẽ elip (vẽ viền)
            gp.DrawEllipse(new Pen(Color.White, (float)(CircleSize)), CircleSize / 2, CircleSize / 2, 
                            CircleDiameter - CircleSize, CircleDiameter - CircleSize);

            final = new Bitmap(final, new Size(170, 170));
            return final;
        }

        /// <summary>
        /// Tạo form Setting và hiển thị form.
        /// </summary>
        /// <returns></returns>
        public static DialogResult Show()
        {
            SF = new SettingForm();
            SF.ShowDialog();

            return DR;
        }

        /// <summary>
        /// Load form đồng thời load các thuộc tính kèm theo.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingForm_Load(object sender, EventArgs e)
        {
            SetProperty();
            if (txtNumberOutLine.Text == "")
            {
                txtNumberOutLine.Enabled = false;
            }
            ImgRoot = Application.StartupPath + "\\avt_root.dat";
        }

        /// <summary>
        /// Đóng form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (IsChangedInfo == true && MessageBox.Show("Bạn có muốn lưu lại những thay đổi không ?", "Thông báo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                btnSave_Click(sender, e);
            }
            else
            {
                this.Close();
            }
        }
        
        /// <summary>
        /// Lưu lại những thay đổi.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsChangedPicture)
            {
                picbAvartar.Image = ImageCrop(lbPath.Text,
                    ckbOutLine.Checked == true ?
                    (txtNumberOutLine.Text == null ? 0 : Int32.Parse(txtNumberOutLine.Text.Trim())) : 0);
            }

            string[] body = new string[7];

            body[0] = lbName.Text.Trim();
            body[1] = lbName.Font.FontFamily.Name;
            body[2] = lbName.Font.Size.ToString();
            body[3] = lbName.Font.Style.ToString();
            body[4] = ckbOutLine.Checked.ToString();
            body[5] = txtNumberOutLine.Text;
            body[6] = lbName.ForeColor.A + " " + lbName.ForeColor.R + " " + 
                        lbName.ForeColor.G + " " + lbName.ForeColor.B;
            
            // Lưu file
            File.WriteAllLines(Application.StartupPath + "\\name.dat", body);

            try
            {
                // Lưu Image đã chỉnh sửa.
                File.Delete(Application.StartupPath + "\\avt.dat");
                picbAvartar.Image.Save(Application.StartupPath + "\\avt.dat", 
                    System.Drawing.Imaging.ImageFormat.Png);

                // Lưu Image gốc.
                Image Img = Image.FromFile(ImgRoot);

                File.Delete(Application.StartupPath + "\\avt_root.dat");
                Img.Save(Application.StartupPath + "\\avt_root.dat",
                    System.Drawing.Imaging.ImageFormat.Png);
            }
            catch
            {
                
            }

            MessageBox.Show("Lưu thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            DR = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }

        /// <summary>
        /// Chọn hình ảnh từ ổ đĩa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChooseImage_Click(object sender, EventArgs e)
        {
            if (openPathDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lbPath.Text = openPathDialog.FileName;
                picbAvartar.Image = ImageCrop(lbPath.Text, ckbOutLine.Checked == true ? 
                    (txtNumberOutLine.Text != "" ? Int32.Parse(txtNumberOutLine.Text.Trim()) : 0) : 0);
                picbAvartar.BackgroundImageLayout = ImageLayout.Stretch;
                IsChangedPicture = true;
                ImgRoot = lbPath.Text;
                IsChangedInfo = true;
            }
        }

        /// <summary>
        /// Thay đổi trạng thái check của checkbox Viền
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbOutLine_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbOutLine.Checked == true)
            {
                txtNumberOutLine.Enabled = true;
                picbAvartar.Image = ImageCrop(ImgRoot, ckbOutLine.Checked == true ?
                        (txtNumberOutLine.Text != "" ? Int32.Parse(txtNumberOutLine.Text.Trim()) : 0) : 0);
            }
            else
            {
                txtNumberOutLine.Enabled = false;
                picbAvartar.Image = ImageCrop(ImgRoot, ckbOutLine.Checked == true ?
                        (txtNumberOutLine.Text != "" ? Int32.Parse(txtNumberOutLine.Text.Trim()) : 0) : 0);
            }
            IsChangedInfo = true;
        }

        /// <summary>
        /// Thay đổi giá trị viền sẽ vẽ lại hình ảnh avartar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumberOutLine_TextChanged(object sender, EventArgs e)
        {
            if (txtNumberOutLine.Text != "")
                if (ValidateNumberOutline())
                {
                    picbAvartar.Image = ImageCrop(ImgRoot, ckbOutLine.Checked == true ?
                            (txtNumberOutLine != null ? Int32.Parse(txtNumberOutLine.Text.Trim()) : 0) : 0);
                }
                else
                {
                    txtNumberOutLine.Text = "100";
                    MessageBox.Show("Dữ liệu không hợp lệ !\n Số đường viền không vượt quá 100 !", "Lỗi", 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            IsChangedInfo = true;
        }

        /// <summary>
        /// Thay đổi tên hiển thị sẽ cập nhật lại tên hiển thị.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if (ValidateName())
            {
                lbName.Text = txtName.Text.Trim();
            }
            else
            {
                MessageBox.Show("Dữ liệu không hợp lệ !\n Tên hiển thị không quá 20 ký tự !", "Lỗi", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Text = txtName.Text.Remove(txtName.Text.Length - 1);
            }
            IsChangedInfo = true;
        }

        /// <summary>
        /// Chọn font sẽ cập nhật lại font chữ mới cho tên hiển thị.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChooseFont_Click(object sender, EventArgs e)
        {
            if (FontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lbName.Font = new Font(new FontFamily(FontDialog.Font.FontFamily.Name), 
                                        FontDialog.Font.Size, FontDialog.Font.Style);
                IsChangedInfo = true;
            }
        }

        /// <summary>
        /// Set cho chương trình chạy cùng hệ thống khi checkbox Startup được check.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbStartUp_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbStartUp.Checked == true)
            {
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\",
                    "SmartSideBarPersonal", 
                    Application.StartupPath + "\\SmartSideBarPersonal.exe", RegistryValueKind.String);
            }
            else
            {
                Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\",
                    true).DeleteValue("SmartSideBarPersonal");
            }
            IsChangedInfo = true;
        }

        /// <summary>
        /// Nhắc nhở giới hạn ký tự trong tên hiển thị.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_Enter(object sender, EventArgs e)
        {
            tltNote.Show("Tên hiển thị có độ dài tối đa là 20 ký tự !", txtName);
        }

        /// <summary>
        /// Ẩn chú thích khi không còn Focus nữa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_Leave(object sender, EventArgs e)
        {
            tltNote.Hide(txtName);
        }

        /// <summary>
        /// Hiển thị nhắc nhở chỉ được phép nhập số và giá trị không quá 100.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumberOutLine_Enter(object sender, EventArgs e)
        {
            tltNote.Show("Chỉ được nhập số và giá trị không quá 100 !", txtNumberOutLine);
        }

        /// <summary>
        /// Ẩn chú thích khi không còn Focus nữa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumberOutLine_Leave(object sender, EventArgs e)
        {
            tltNote.Hide(txtNumberOutLine);
        }

        /// <summary>
        /// Event ngăn chặn tất cả các phím trừ phím số và phím Backspace.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumberOutLine_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || Convert.ToInt32(e.KeyChar) == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Chọn màu sẽ cập nhật lại màu chữ cho chữ hiển thị.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChooseColor_Click(object sender, EventArgs e)
        {
            if (ColorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lbName.ForeColor = ColorDialog.Color;
                IsChangedInfo = true;
            }
        }
    }
}
