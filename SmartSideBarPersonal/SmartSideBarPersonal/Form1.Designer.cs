﻿namespace SmartSideBarPersonal
{
    partial class SmartSideBarPersonal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picbAvartar = new System.Windows.Forms.PictureBox();
            this.lbName = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picbAvartar)).BeginInit();
            this.SuspendLayout();
            // 
            // picbAvartar
            // 
            this.picbAvartar.BackColor = System.Drawing.Color.Transparent;
            this.picbAvartar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picbAvartar.Location = new System.Drawing.Point(77, 12);
            this.picbAvartar.Name = "picbAvartar";
            this.picbAvartar.Size = new System.Drawing.Size(170, 170);
            this.picbAvartar.TabIndex = 0;
            this.picbAvartar.TabStop = false;
            this.picbAvartar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picbAvartar_MouseDown);
            this.picbAvartar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picbAvartar_MouseMove);
            this.picbAvartar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picbAvartar_MouseUp);
            // 
            // lbName
            // 
            this.lbName.BackColor = System.Drawing.Color.Transparent;
            this.lbName.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbName.Location = new System.Drawing.Point(1, 187);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(325, 46);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "TRẦN THÀNH VI THANH";
            this.lbName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbName.Click += new System.EventHandler(this.lbName_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.SystemColors.Control;
            this.btnThoat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThoat.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.btnThoat.Location = new System.Drawing.Point(299, -1);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(29, 32);
            this.btnThoat.TabIndex = 2;
            this.btnThoat.Text = "X";
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Visible = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SmartSideBarPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 240);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.picbAvartar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SmartSideBarPersonal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Smart SideBar Personal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.SmartSideBarPersonal_Load);
            this.MouseEnter += new System.EventHandler(this.SmartSideBarPersonal_MouseEnter);
            ((System.ComponentModel.ISupportInitialize)(this.picbAvartar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picbAvartar;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Timer timer1;
    }
}

