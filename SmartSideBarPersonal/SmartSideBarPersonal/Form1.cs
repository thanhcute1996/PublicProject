﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace SmartSideBarPersonal
{
    public partial class SmartSideBarPersonal : Form
    {
        private bool Move = false;
        private int x, y;

        public SmartSideBarPersonal()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Get Background Image at Application Location.
        /// </summary>
        /// <returns>Return a Image</returns>
        private Image GetBackground()
        {
            this.Hide();
            Bitmap bm = new System.Drawing.Bitmap(this.Width, this.Height);
            Graphics gp = Graphics.FromImage(bm);

            gp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gp.CopyFromScreen(this.DesktopLocation.X + 
                                Screen.PrimaryScreen.WorkingArea.X, 
                                this.DesktopLocation.Y + 
                                Screen.PrimaryScreen.WorkingArea.Y, 0, 0, new Size(this.Width, this.Height));

            this.Show();

            return bm;
        }

        /// <summary>
        /// Đặt lại giá trị các thuộc tính.
        /// </summary>
        private void SetProperty()
        {
            try
            {
                // Load avartar
                Image img = Image.FromStream(new MemoryStream(File.ReadAllBytes(Application.StartupPath + "\\avt.dat")));
                picbAvartar.Image = img;

                // Load tên hiển thị và style
                string[] body = File.ReadAllLines(Application.StartupPath + "\\name.dat");
                lbName.Text = body[0];
                lbName.Font = new System.Drawing.Font(new FontFamily(body[1]), float.Parse(body[2]), body[3] == "Bold" ? 
                    FontStyle.Bold : (body[3] == "Italic" ? FontStyle.Italic : System.Drawing.FontStyle.Regular));

                // Load color cho text
                string[] TextColor = body[6].Split(' ');
                lbName.ForeColor = Color.FromArgb(Int32.Parse(TextColor[0]), Int32.Parse(TextColor[1]), Int32.Parse(TextColor[2]), Int32.Parse(TextColor[3]));
                
                // Set vị trí cho app
                string[] location = File.ReadAllLines(Application.StartupPath + "\\location.dat");
                this.SetDesktopLocation(Int32.Parse(location[0]), Int32.Parse(location[1]));

            }
            catch
            {
                MessageBox.Show(" Load dữ liệu không thành công !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Event xảy ra khi nhấn chuột vào hình ảnh mà chưa thả ra.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picbAvartar_MouseDown(object sender, MouseEventArgs e)
        {
            Move = true;
            x = e.X;
            y = e.Y;
        }

        /// <summary>
        /// Event xảy ra khi thả chuột ra.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picbAvartar_MouseUp(object sender, MouseEventArgs e)
        {
            Move = false;

            this.BackgroundImage = GetBackground();
        }

        /// <summary>
        /// Event xảy ra khi di chuyển chuột.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picbAvartar_MouseMove(object sender, MouseEventArgs e)
        {
            if (Move)
            {
                this.SetDesktopLocation(Cursor.Position.X - x - Screen.PrimaryScreen.WorkingArea.X 
                                        - picbAvartar.Location.X, Cursor.Position.Y - y 
                                        - Screen.PrimaryScreen.WorkingArea.Y - picbAvartar.Location.Y);

                //this.BackgroundImage = GetBackground();
                //System.Threading.Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Mở thiết lập hiển thị.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbName_Click(object sender, EventArgs e)
        {
            if (SettingForm.Show() == System.Windows.Forms.DialogResult.OK)
            {
                GetBackground();
            }
            SetProperty();
        }

        /// <summary>
        /// Lưu lại vị trí của Application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            string[] location = new string[2];
            location[0] = this.DesktopLocation.X.ToString();
            location[1] = this.DesktopLocation.Y.ToString();
            File.WriteAllLines(Application.StartupPath + "\\location.dat", location);
        }

        /// <summary>
        /// Set các thuộc tính cũ và get background.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SmartSideBarPersonal_Load(object sender, EventArgs e)
        {
            SetProperty();
            this.BackgroundImage = GetBackground();
        }

        private void SmartSideBarPersonal_MouseEnter(object sender, EventArgs e)
        {
            btnThoat.Visible = true;
            timer1.Enabled = true;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SmartSideBarPersonal_MouseLeave(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (btnThoat.Visible == true)
            {
                btnThoat.Visible = false;
                timer1.Enabled = false;
            }
        }
    }
}
