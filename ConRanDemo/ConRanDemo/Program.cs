﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace ConRanDemo
{
    class Program
    {
        struct ConRan
        {
            public int x;
            public int y;
        }

        #region
        //Console.ForegroundColor = ConsoleColor.Black;
        //VeRan(R, ChieuDai);
        //Console.Clear();
        #endregion

        #region Hằng số

        const int ChieuDaiKhung = 70;
        const int ChieuRongKhung = 23;
        const int BienTrai = 6;
        const int BienPhai = 73;
        const int BienTren = 0;
        const int BienDuoi = 23;

        #endregion

        static void KhoiTaoRan(ConRan[] R, int ChieuDai)
        {
            // Ô đầu tiên của khung là (6, 1)
            for (int i = 0; i < ChieuDai; i++)
            {
                R[i].x = 7;
                R[i].y = 1;
            }
        }

        static void VeRan(ConRan[] R, int len)
        {
            for (int i = 0; i < len; i++)
            {
                Console.SetCursorPosition(R[i].x, R[i].y);
                Console.Write("*");
            }
        }

        static bool AnMoi(ConRan[] R, int x, int y)
        {
            if (R[0].x == x && R[0].y == y)
                return true;
            else
                return false;
        }

        static void XuatHienMoi(ConRan[] R, ref int x, ref int y)
        {
            Random rd = new Random();
            if (AnMoi(R, x, y))
            {
                x = rd.Next(9, 69);
                y = rd.Next(2, 22);
                if (R[0].x == x && R[0].y == y)
                    x++;
            }

            Console.SetCursorPosition(x, y);
            Console.Write("*");
        }

        static void Main(string[] args)
        {
            if (args.Length >= 3)
            {
                Console.WriteLine(args[0] + "\n" + args[1] + "\n" + args[2]);
            }

            if (KPlusAuthentication.Authentication.Instance.CheckAuthentication(args[0], args[1], args[2]) == false || args.Length < 3)
            {
                Console.WriteLine("Kiem tra ban quyen that bai!");
                Console.ReadLine();
                Environment.Exit(0);
            }
            else
            {

                Console.WriteLine("Kiem tra thanh cong! Nhan enter de bat dau!");
                Console.ReadLine();
                Console.Clear();
            }

            ConRan[] R = new ConRan[100];
            int ChieuDai = 5, x = 1, y = 0, xx = 8, yy = 8; // chiều dài ban đầu của con rắn, xx và yy là toạ độ của mồi
            char k = ' ';
            int Delay = 200;

            #region Vẽ khung
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(5, 0);

            for (int i = 0; i < ChieuDaiKhung; i++)
                Console.Write("=");

            for (int i = 0; i < ChieuRongKhung; i++)
            {
                Console.SetCursorPosition(5, i + 1);
                Console.Write("||");
                Console.SetCursorPosition(ChieuDaiKhung + 3, i + 1);
                Console.WriteLine("||");
            }
            Console.SetCursorPosition(5, ChieuRongKhung);
            for (int i = 0; i < ChieuDaiKhung; i++)
                Console.Write("=");
            #endregion
            KhoiTaoRan(R, ChieuDai);

            do
            {
                #region
                Console.SetCursorPosition(R[ChieuDai - 1].x, R[ChieuDai - 1].y);
                Console.Write(" ");
                #endregion
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo ckf = Console.ReadKey(true);
                    k = ckf.KeyChar;
                }

                #region Xử lý phím nhấn
                if (k == 'a')
                {
                    x = -1;
                    y = 0;
                }

                if (k == 'd')
                {
                    x = 1;
                    y = 0;
                }

                if (k == 'w')
                {
                    x = 0;
                    y = -1;
                }

                if (k == 's')
                {
                    x = 0;
                    y = 1;
                }
                #endregion

                #region Xử lý rắn
                for (int i = ChieuDai - 1; i > 0; i--)
                    R[i] = R[i - 1];
                R[0].x = R[0].x + x;
                R[0].y = R[0].y + y;
                #endregion

                #region Xử lý lỗi
                if (R[0].x == BienTrai)
                {
                    R[0].x = BienPhai - 1;
                }
                if (R[0].x == BienPhai)
                {
                    R[0].x = BienTrai + 1;
                }
                if (R[0].y == BienTren)
                {
                    R[0].y = BienDuoi - 1;
                }
                if (R[0].y == BienDuoi)
                {
                    R[0].y = BienTren + 1;
                }
                #endregion

                #region Vẽ rắn và vẽ mồi
                Console.ForegroundColor = ConsoleColor.White;
                VeRan(R, ChieuDai);
                if (AnMoi(R, xx, yy))
                {
                    ChieuDai++;
                    Delay -= 5;
                    //for (int i = ChieuDai - 1; i > 0; i--)
                    //    R[i] = R[i - 1];
                    //R[0].x = xx;
                    //R[0].y = yy;
                }

                XuatHienMoi(R, ref xx, ref yy);
                #endregion

                if (Delay > 0)
                    System.Threading.Thread.Sleep(Delay);
                else
                    System.Threading.Thread.Sleep(20);
            }
            while (k != 27);

            Console.ReadLine();
        }
    }
}
