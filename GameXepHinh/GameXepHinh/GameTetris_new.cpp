#include <iostream>
#include "RequireFunc.h"
#include <Windows.h>
#include <time.h>
using namespace std;
#define MaxI 24
#define MaxJ 12
#define LEFT 5
#define TOP 5

const int L=20, T=15, Ex1 =40, Ex2=10; // There are some useful const nums
char board[MaxI][MaxJ];

void Setvalue()
{
	//board
	int i,j;
	for(i=0;i<MaxI;i++)
		{
			for(j =0;j < MaxJ;j++)
				{
					if(i==2 || i ==MaxI-1 && j>0 && j<MaxJ)
					board[i][j] = '-';
					if((j==0 || j == MaxJ-1) && (i>2 && i<MaxI))
					board[i][j] = '-';
				}
		}
}

typedef struct material
{
	char **a;
	int brickI,brickJ;
	int RbrickI,RbrickJ;
};

void Allocation(char **& a,int y, int x) // Allocate 2d array
{
	int i;
	a = new char *[y];
	for(i = 0;i< y;i++)
		a[i] = new char [x];
}

material Buildbrick(int ID)
{
	material brick;
	int i,j;
	switch(ID)
	{
		case 1: //Rectangle
			brick.brickI = 4; 
			brick.brickJ = 1; 
			brick.RbrickI = 3;
			brick.RbrickJ = 5;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					brick.a[i][j] = '@';
				}
			break;
		case 2: // cubic L (LEFT)
			brick.brickI = 3;
			brick.brickJ = 2;
			brick.RbrickI = 3;
			brick.RbrickJ = 5;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					if(j==1 && (i==0||i==1))
						brick.a[i][j] = 32;
					else
						brick.a[i][j] = '@';
				}
			break;
			case 3: // cubic Z(LEFT)
			brick.brickI = 2;
			brick.brickJ = 3;
			brick.RbrickI = 3;
			brick.RbrickJ = 4;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					if((i==1 && j==0)|| (i==0&&j==2))
						brick.a[i][j] = 32;
					else
						brick.a[i][j] = '@';
				}
			break;
			case 4: // cubic Z(Right)
			brick.brickI = 2;
			brick.brickJ = 3;
			brick.RbrickI = 3;
			brick.RbrickJ = 4;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					if((i==1 && j==2)|| (i==0&&j==0))
						brick.a[i][j] = 32;
					else
						brick.a[i][j] = '@';
				}
			break;
			case 5: // Square
			brick.brickI = 2;
			brick.brickJ = 2;
			brick.RbrickI = 3;
			brick.RbrickJ = 4;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
						brick.a[i][j] = '@';
				}
			break;
			case 6: // Cubic L(RIGHT)
			brick.brickI = 2;
			brick.brickJ = 3;
			brick.RbrickI = 3;
			brick.RbrickJ = 4;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					if(i==0 && (j==1||j==2))
						brick.a[i][j] = 32;
					else
						brick.a[i][j] = '@';
				}
			break;
			case 7: // Cubic T
			brick.brickI = 2;
			brick.brickJ = 3;
			brick.RbrickI = 3;
			brick.RbrickJ = 4;
			Allocation(brick.a,brick.brickI,brick.brickJ);
			for(i = 0; i< brick.brickI;i++)
				for( j = 0; j < brick.brickJ;j++)
				{
					if(i==0 && (j==0||j==2))
						brick.a[i][j] = 32;
					else
						brick.a[i][j] = '@';
				}
			break;
	}
return brick;
}

void Update_Coordinate(int row)
{
    int i,j;
    for(i=row;i>0;i--)
        for(j=0;j<MaxJ;j++)
        {
            board[i][j]=board[i-1][j];
        }
}

int Inside(int i,int j) 
{
    return (i>=0&&i<MaxI&&j>=0&&j<MaxJ);
}

int rightcond(int i,int j)
{
	if(board[i][j+1] != 32 || j+2 == MaxJ)
		return 0;
return 1;
}

int leftcond(int i,int j)
{
	if(board[i][j-1] != 32 || j-1 == 0 )
		return 0;
return 1;
}

int downcond(int i,int j)
{
	if((board[i+1][j] ==32)&& i< MaxI-1&&Inside(i,j)==1)
		return 1;
	else
		return 0;
}

void Right(material &Brick)
{
	int i,j;
	for(i=0;i<Brick.brickI;i++)
		for(j=0;j<Brick.brickJ;j++)
		{
			if(Brick.a[i][j] == '@')
				{
					if(rightcond(i+Brick.RbrickI,j+ Brick.RbrickJ) == 0)
						return;
				}
		}

Brick.RbrickJ++;
}

void Left(material &Brick)
{
	int i,j;
	for(i=0;i<Brick.brickI;i++)
		for(j=0;j<Brick.brickJ;j++)
		{
			if(Brick.a[i][j] == '@')
				if(leftcond(i+Brick.RbrickI,j+ Brick.RbrickJ) == 0)
					return;
		}
Brick.RbrickJ--;
}

int Down(material &Brick)
{
	int i,j;
	for(i=0;i<Brick.brickI;i++)
		for(j=0;j<Brick.brickJ;j++)
		{
			if(Brick.a[i][j] == '@')
				{
					if(downcond(i+Brick.RbrickI,j+Brick.RbrickJ) == 0)
						return 0;
				}
		}	
Brick.RbrickI++;
return 1;
}

void Ronate(material &Brick)
{
	int i,j,Tp = 0;
	char **tmp;
	Allocation(tmp,Brick.brickJ,Brick.brickI); // When ronating, its column and row will be changed
	//Ronate
	for(i=0;i<Brick.brickI;i++)
		for(j=0;j<Brick.brickJ;j++)
			tmp[j][Brick.brickI-i-1] = Brick.a[i][j];
	//Check condition
	for(i=0;i<Brick.brickJ;i++)
		for(j=0;j<Brick.brickI;j++)
		{
			if(board[Brick.RbrickI+i][Brick.RbrickJ+j] != 32)
				return;
		}
	for(i=0 ;i<Brick.brickI ;i++)
		delete []Brick.a[i];
	delete [] Brick.a;
	//=====================================================================
	Tp = Brick.brickI;
	Brick.brickI = Brick.brickJ;
	Brick.brickJ = Tp;
	Brick.a = tmp;
}

int RandomID()
{
	srand(time(NULL));
	 return 1 + rand() % (7-1+1);
}

void RemoveLeaks(material Brick)
{
	for(int i=0 ;i < Brick.brickI;i++)
		delete []Brick.a[i];
	delete [] Brick.a;
}
//================================================================
//Game_system.
//Info.
int Getkey_when_starting_game(int &a)
{
	if(GetAsyncKeyState(VK_UP))
	{
		a = 0;
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)-2));textcolor(124);cout<<"START";
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)+2));textcolor(14);cout<<"EXIT";
	}
	if(GetAsyncKeyState(VK_DOWN ))
	{
		a =1;
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)-2));textcolor(14);cout<<"START";
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)+2));textcolor(124);cout<<"EXIT";
	}
	if(a  == 1 && GetAsyncKeyState(VK_SPACE))
		return 0;
	if(a  == 0 && GetAsyncKeyState(VK_SPACE))
		{
			gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)-2));textcolor(6);cout<<"     ";
			return 1;
		}
return -1;
}

void start_Game()
{
	//=================================================================
	gotoxy(L,T-10);cout<<"_____ _____ _____ ____  ___ ____ ";		  //
	gotoxy(L,T-9);cout<<"|_   _| ____|_   _|  _ \|_ _/ ___|";		  //
	gotoxy(L,T-8);cout<<"  | | |  _|   | | | |_) || |\___ \ ";		  //
	gotoxy(L,T-7);cout<<"  | | | |___  | | |  _ < | | ___) |";		  //
	gotoxy(L,T-6);cout<<"  |_| |_____| |_| |_| \_\___|____/ ";		  //
	gotoxy(L,T-5);cout<<"       By: Nguyen Minh Khue        ";		  //
	gotoxy(L,T-4);cout<<"University Of Information Technology";		  //
	//=================================================================
	int i,j;
	for (i = L+5; i <= L+Ex1-10;i++)
	{
		for(j = T; j <= T+Ex2;j++)
		{

			if( i == L+5 || i == L+Ex1-10 && j >T && j < T+Ex2)
			{
				gotoxy(i,j);textcolor(3);cout << (char)186;
			}
			if( j == T || j == T+Ex2 && i >= L+5 && i <= L+Ex1-10)
			{
				gotoxy(i,j);textcolor(3);cout << (char)205;
			}
		}
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)-2));textcolor(14);cout<<"START";
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)+2));textcolor(14);cout<<"EXIT";
		gotoxy(L+5,T);textcolor(3);cout << (char)201;
		gotoxy(L-10 + Ex1,T);textcolor(3);cout << (char)187;
		gotoxy(L-10 + Ex1,T + Ex2);textcolor(3);cout << (char)188;
		gotoxy(L+5,T + Ex2);textcolor(3);cout << (char)200;
	}
}

typedef struct Infor
{
	int level;
	int score,lines;
	float speed;
};

void Init(Infor infor)
{
    infor.level=1;
    infor.score=0;
    infor.speed=0.3;
}

int check_each_line(int i)
{
	int j;
	for(j=1;j<MaxJ;j++)
		{
			if(board[i][j] == 32)
				return 0;
		}
return 1;
}

void referch()
{
	for(int i=0;i<MaxI-1;i++)
		for(int j=1;j<MaxJ-1;j++)
			{
				if(board[i][j]!='@'&& i > 1)
					board[i][j]=32;
			}
}

int Result(material &Brick,Infor &infor)
{
	int i = Brick.brickI-1,j,Check=0;
	if(Brick.RbrickI<=3)
		return 0; //GameOver
	do
	{
		Check = check_each_line(i+Brick.RbrickI);
		if(Check == 1)
		{
			Update_Coordinate(i+Brick.RbrickI);
			infor.score+=10;
			infor.lines+=1;
			if(infor.score >= 100&&infor.score<200&&infor.level<2)
			{
				infor.level++;
				infor.speed-=0.1;
			}
			if(infor.score >= 200&&infor.level<3)
			{
				infor.level++;
				infor.speed-=0.1;
			}
			referch();
			Setvalue();
		}
		else
			i--;
	}while(i>=0);
return 1;
}

void Scoreboard(Infor &infor)
{
    textcolor(15);
	gotoxy(LEFT+MaxJ+2,16);
	cout<< "LEVEL:           "<<infor.level;
	gotoxy(LEFT+MaxJ+2,20);
	cout<< "SCORE:           "<<infor.score;
	gotoxy(LEFT+MaxJ+2,18);
	cout<< "LINES:           "<<infor.lines;
}

void Decoration()
{
	textcolor(14);
	gotoxy(LEFT+MaxJ+2,25);cout<<" _____ ___ _____ ___ ___ ___ ";
	gotoxy(LEFT+MaxJ+2,26);cout<<"|_   _| __|_   _| _ \_ _/ __|";
	gotoxy(LEFT+MaxJ+2,27);cout<<"  | | | _|  | | |   /| |\__ \ ";
	gotoxy(LEFT+MaxJ+2,28);cout<<"  |_| |___| |_| |_|_\___|___/";
}

void Brick_Board()
{
	int i,j;
	for(i=LEFT+MaxJ+2;i<=LEFT+MaxJ+20;i++)
		{
			for(j=TOP+2;j<=14;j++)
			{
				if( i == LEFT+MaxJ+2 || i == LEFT+MaxJ+20 && j >TOP+2 && j < 14)
				{
					gotoxy(i,j);textcolor(11);cout << (char)186;
				}
				if( j == TOP+2 || j == 14 && i >= LEFT+MaxJ+2 && i <= LEFT+MaxJ+20)
				{
					gotoxy(i,j);textcolor(11);cout << (char)205;
				}
			}
		gotoxy(LEFT+MaxJ+2,TOP+2);textcolor(11);cout << (char)201;
		gotoxy(LEFT+MaxJ+2,14);textcolor(11);cout << (char)200;
		gotoxy(LEFT+MaxJ+20,14);textcolor(11);cout << (char)188;
		gotoxy(LEFT+MaxJ+20,TOP+2);textcolor(11);cout << (char)187;

		}
}

void Next_Brick(int ID)
{
	material Brick = Buildbrick(ID);
	int i,j;
	for(i = 0;i<Brick.brickI;i++)
		for(j=0;j<Brick.brickJ;j++)
		{
			if(Brick.a[i][j]  =='@')
			{
				if(ID==1)
					gotoxy(LEFT+MaxJ+11+j,9+i);
				else
				gotoxy(LEFT+MaxJ+10+j,10+i);
				textcolor(13);
				cout << Brick.a[i][j];
			}
		}
	RemoveLeaks(Brick);
}

void Delete_TheNextBrick()
{
	int i,j;
	for(i = 0;i<=4;i++)
		for(j=0;j<4;j++)
		{
			gotoxy(LEFT+MaxJ+10+j,9+i);textcolor(0);
			cout << (char)32;
		}
}
//================================================================
//================================================================
void delbrick(material brick)
{
	int i,j;
	for(i = 0;i<brick.brickI;i++)
		for(j=0;j<brick.brickJ;j++)
		{
			if(brick.a[i][j] == '@')
			{
				textcolor(0);
				gotoxy(j+LEFT+brick.RbrickJ,i+TOP+brick.RbrickI);
				cout << (char)32;
			}
		}
}
void print(material brick)
{
	int i,j;
	for(i = 0;i<brick.brickI;i++)
		for(j=0;j<brick.brickJ;j++)
		{
			if(brick.a[i][j]  =='@')
			{
				textcolor(14); 
				gotoxy(j+LEFT+brick.RbrickJ,i+TOP+brick.RbrickI);
				cout << brick.a[i][j];
			}
		}

}
void setvalue_2(material &Brick,int &flag,Infor &infor,int& Rand) //Save the brick to game board
{
	int i,j;
	if(Down(Brick)==0)
	{
		for(i=0;i<Brick.brickI;i++)
			for(j=0;j<Brick.brickJ;j++)
				{
					if(Brick.a[i][j] != 32)
						board[i+Brick.RbrickI][j+Brick.RbrickJ] = Brick.a[i][j];
				}
		flag = Result(Brick,infor);
		Scoreboard(infor);
		RemoveLeaks(Brick);
		Brick = Buildbrick(Rand);
		Rand = RandomID();
		Delete_TheNextBrick();
		Next_Brick(Rand);
	}
}
void GameOver(Infor INF)
{
	int i,j;
	for (i = L; i <= L+Ex1;i++)
	{
		for(j = T; j <= T+Ex2;j++)
		{

			if( i == L || i == L+Ex1 && j >T && j < T+Ex2)
			{
				gotoxy(i,j);textcolor(3);cout << (char)186;
			}
			if( j == T || j == T+Ex2 && i >= L && i <= L+Ex1)
			{
				gotoxy(i,j);textcolor(3);cout << (char)205;
			}
		}
		gotoxy((L+(Ex1/2-4)),(T+(Ex2/2)));textcolor(5);cout<<"GAME OVER";
		gotoxy((L+(Ex1/2-6)),(T+(Ex2/2)+1));textcolor(14);cout<< "Your Score: "<<INF.score;
		gotoxy((L+(Ex1/2-12)),(T+(Ex2/1.25)));textcolor(5);cout<<"Retry";
		gotoxy((L+(Ex1/2+8)),(T+(Ex2/1.25)));textcolor(5);cout<<"Exit";
		gotoxy(L,T);textcolor(3);cout << (char)201;
		gotoxy(L + Ex1,T);textcolor(3);cout << (char)187;
		gotoxy(L + Ex1,T + Ex2);textcolor(3);cout << (char)188;
		gotoxy(L,T + Ex2);textcolor(3);cout << (char)200;
	}
}
int Getkeyinthelastgame(int &a)
{
	if(GetAsyncKeyState(VK_LEFT))
	{
		a = 0;
		gotoxy((L+(Ex1/2-12)),(T+(Ex2/1.25)));textcolor(124);cout<<"Retry";
		gotoxy((L+(Ex1/2+8)),(T+(Ex2/1.25)));textcolor(5);cout<<"Exit";
	}
	if(GetAsyncKeyState(VK_RIGHT))
	{
		a =1;
		gotoxy((L+(Ex1/2+8)),(T+(Ex2/1.25)));textcolor(124);cout<<"Exit";
		gotoxy((L+(Ex1/2-12)),(T+(Ex2/1.25)));textcolor(5);cout<<"Retry";
	}
	if(a  == 1 && GetAsyncKeyState(VK_SPACE))
		return 0;
	if(a  == 0 && GetAsyncKeyState(VK_SPACE))
		{
			gotoxy((L+(Ex1/2-12)),(T+(Ex2/1.25)));textcolor(0);cout<<"     ";
			return 1;
		}
return -1;
}
//====================================================

void GetKey(material &Brick)
{
	delbrick(Brick);
	if(GetAsyncKeyState(VK_UP))
	{
		Ronate(Brick);
	}
	if(GetAsyncKeyState(VK_RIGHT))
	{
		Right(Brick);
	}
	if(GetAsyncKeyState(VK_LEFT))
	{
		Left(Brick);
	}
	if(GetAsyncKeyState(VK_DOWN))
	{
		int k = Down(Brick);
	}
	print(Brick);
}
void Print_GBoard()
{
	int i,j;
	textcolor(7);
	for(i=0;i<MaxI;i++)
		{
			for(j =0;j < MaxJ;j++)
				{
					gotoxy(LEFT+j,TOP+i);
					if(board[i][j] == '-')
						cout << (char)178;
					else
						cout << board[i][j];
				}
		}
}

int main()
{
	int RE = -1,zero;
	start_Game();
	while(true)
	{
		zero = Getkey_when_starting_game(RE);
		if(zero==1)
			break;
		if(zero==0)
			goto END;
		
	} 
	RETRY:
	clrscr();
	int Flag=1,sign = -1,Random;
	float Start,End;
	Infor infor;
	infor.level=1;
    infor.score=0;
	infor.lines=0;
    infor.speed=0.3;
	resizeConsole(800, 600);
	material yourbrick;
	Scoreboard(infor);
	Decoration();
	//assign space to game board.
	for(int i=0;i<MaxI;i++)
		for(int j=0;j<MaxJ;j++)
			board[i][j]=32;
	Random = RandomID();
	yourbrick = Buildbrick(Random);
	Brick_Board();
	Random = RandomID();
	Next_Brick(Random);
	while(Flag ==1)
	{
		Setvalue();
		Print_GBoard();
		print(yourbrick);
		referch();
		Start = clock();
		do
		{
			GetKey(yourbrick);
			Sleep(80);
			End = clock();
		}
		while ((float)(End - Start)/CLOCKS_PER_SEC < infor.speed);
		setvalue_2(yourbrick,Flag,infor,Random);
		
	}
	clrscr();
	int a = -1;
	if(Flag ==0)
		{
			GameOver(infor);
			cout<<endl;
			while(sign ==-1)
				{
					sign = Getkeyinthelastgame(a);
				}
			if(sign ==1)
				goto RETRY;
		}
END:
return 0;
}