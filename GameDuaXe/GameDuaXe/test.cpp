#include<iostream>
#include<Windows.h>
#include<string>
#include<time.h>
//#include"dohoa.h"
using namespace std;
void resizeConsole(int width, int height)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, width, height, TRUE);
}
void textcolor(int x)
{
	HANDLE mau;
	mau=GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(mau,x);
}
void gotoxy(int x,int y)
{    
	HANDLE hConsoleOutput;    
	COORD Cursor_an_Pos = {x-1,y-1};   
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);    
	SetConsoleCursorPosition(hConsoleOutput , Cursor_an_Pos);
}
 
void XoaManHinh()
{
	HANDLE hOut;
	COORD Position;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	Position.X = 0;
	Position.Y = 0;
	SetConsoleCursorPosition(hOut, Position);
}
void set_up_road(char map[][50])
{
	for(int i = 0; i < 30; i++)
	{
		for(int j=0;j<30;j++)
		{
			map[i][j]=' ';
		}
	}
	for(int i=0;i<30;i++)
	{
		map[i][0]='|';
		map[i][14]='|';
		map[i][29]='|';
	}
}
void print_road(char map[][50])
{
	for(int i = 0; i < 30; i++)
	{
		for(int j = 0; j < 30; j++)
		{
			 if(map[i][j]=='!')
			 {
				 textcolor(160);
				 cout<<map[i][j];
			 }
			 else if(map[i][j]== 'O'||map[i][j]=='#'||map[i][j]=='X')
			 {
				 textcolor(220);
				 cout<<map[i][j];
			 }
			 else
			 {
			  textcolor(26);
			  cout<<map[i][j];
			  textcolor(7);
			 }
 
		}
		cout<<endl;
	}
 
	textcolor(7);
}
void car(char map[][50],int row, int column)
{
 
	map[row][column] = 'X'; 
	map[row][column - 1] = '#'; 
	map[row][column + 1] = '#'; 
	map[row - 1][column - 1] = 'O'; 
	map[row + 1][column - 1] = 'O'; 
	map[row - 1][column + 1] = 'O';
	map[row + 1][column + 1] = 'O'; 
 
}
void car_computer(char map[][50],int row, int column)
{
 
	map[row][column] = '!'; 
	map[row][column - 1] = '!'; 
	map[row][column + 1] = '!'; 
	map[row - 1][column - 1] = '!'; 
	map[row + 1][column - 1] = '!'; 
	map[row - 1][column + 1] = '!';
	map[row + 1][column + 1] = '!'; 
 
}
void car_computer_1(char map[][50],int row, int column)
{
	map[row][column] = '!'; 
	map[row][column - 1] = '!'; 
	map[row][column + 1] = '!'; 
	map[row - 1][column - 1] = '!'; 
	map[row + 1][column - 1] = '!'; 
	map[row - 1][column + 1] = '!';
	map[row + 1][column + 1] = '!'; 
 
}
void car_computer_2(char map[][50],int row, int column)
{
	map[row][column] = '!'; 
	map[row][column - 1] = '!'; 
	map[row][column + 1] = '!'; 
	map[row - 1][column - 1] = '!'; 
	map[row + 1][column - 1] = '!'; 
	map[row - 1][column + 1] = '!';
	map[row + 1][column + 1] = '!'; 
 
}
void delete_car_computer(char map[][50],int row, int column)
{
	map[row][column] = ' '; 
	map[row][column - 1] = ' '; 
	map[row][column + 1] = ' '; 
	map[row - 1][column - 1] = ' '; 
	map[row + 1][column - 1] = ' '; 
	map[row - 1][column + 1] = ' ';
	map[row + 1][column + 1] = ' '; 
	for(int i=0;i<30;i++)
	{
		//if(i%2!=0) map[i][14]=' ';
        map[i][14]='|';
		map[i][0]='|';
		//map[i][14]='|';
		map[i][29]='|';
	}
 
}
void delete_car_computer_2(char map[][50],int row, int column)
{
	map[row][column] = ' '; 
	map[row][column - 1] = ' '; 
	map[row][column + 1] = ' '; 
	map[row - 1][column - 1] = ' '; 
	map[row + 1][column - 1] = ' '; 
	map[row - 1][column + 1] = ' ';
	map[row + 1][column + 1] = ' '; 
	for(int i=0;i<30;i++)
	{
		//if(i%2!=0) map[i][14]=' ';
        map[i][14]='|';
		map[i][0]='|';
		//map[i][14]='|';
		map[i][29]='|';
	}
}
void delete_car_computer_1(char map[][50],int row, int column)
{
	map[row][column] = ' '; 
	map[row][column - 1] = ' '; 
	map[row][column + 1] = ' '; 
	map[row - 1][column - 1] = ' '; 
	map[row + 1][column - 1] = ' '; 
	map[row - 1][column + 1] = ' ';
	map[row + 1][column + 1] = ' '; 
	for(int i=0;i<30;i++)
	{
		//if(i%2!=0) map[i][14]=' ';
        map[i][14]='|';
		map[i][0]='|';
		//map[i][14]='|';
		map[i][29]='|';
	}
 
}
void delete_car(char map[][50],int row, int column)
{
	map[row][column] = ' '; 
	map[row][column - 1] = ' '; 
	map[row][column + 1] = ' '; 
	map[row - 1][column - 1] = ' '; 
	map[row + 1][column - 1] = ' '; 
	map[row - 1][column + 1] = ' ';
	map[row + 1][column + 1] = ' '; 
	for(int i=0;i<30;i++)
	{
		//if(i%2!=0) map[i][14]=' ';
        map[i][14]='|';
		map[i][0]='|';
		//map[i][14]='|';
		map[i][29]='|';
	}
}
void introduce()
{
	string str1="===============>Console Game<==================";
	string str2="-----------------RANCING GAME------------------";
	string str3="______________________\3UIT\3___________________";
	for(int i=0;i<str1.size();i++)
	{
		Sleep(100);
		textcolor(158);
		cout<<str1[i];
		textcolor(7);
	}
	cout<<endl;
	for(int i=0;i<str2.size();i++)
	{
		Sleep(100);
		textcolor(158);
		cout<<str2[i];
		textcolor(7);
	}
	cout<<endl;
	for(int i=0;i<str3.size();i++)
	{
		Sleep(100);
		textcolor(158);
		cout<<str3[i];
		if(i==str3.size()-1) Sleep(2000);
		textcolor(7);
	}
	cout<<endl;
	system("cls");
}
/*void test(char map[][50],int x,int y,int &life)
{
	if(map[x][y]== '!'||map[x][y - 1] =='!'||map[x][y + 1] == '!'||map[x - 1][y - 1] =='!'||map[x + 1][y - 1] =='!'||map[x - 1][y + 1] =='!'||map[x + 1][y + 1] =='!')
	{
		cout<<"\a";
		life--;
	}
}*/
int main()
{ 
	introduce();
cuong:
	int long long diem=0;
	int lever=0;
	int life=3;
	textcolor(9);
	gotoxy(25,10);
	cout<<"3";
	Sleep(1000);
	XoaManHinh();
	gotoxy(25,10);
	cout<<"2";
	Sleep(1000);
	XoaManHinh();
	gotoxy(25,10);
	cout<<"1";
	Sleep(1000);
	XoaManHinh();
	gotoxy(25,10);
	cout<<"Go";
	Sleep(1000);
	XoaManHinh();
	textcolor(7);
cuongpham:
	char map[50][50];
	int x=28,y=14;
	int x1=2,y1=10;
	int x2=6,y2=20;
	int x3=4,y3=14;
	resizeConsole(650,500);
	set_up_road(map);
	car_computer(map,x1,y1);
	car_computer_1(map,x2,y2);
	car_computer_2(map,x3,y3);
	car(map,x,y);
	print_road(map);
	textcolor(10);
	cout<<"Life : "<<life<<endl;
	textcolor(11);
	cout<<"Lever : "<<diem/2500<<endl;
	textcolor(12);
	cout<<"Scores : "<<diem<<endl;;
	gotoxy(32,2);
	string intro="---->>Build By Cuong Pham UIT K11<<----";
	string intro1="______Game Version :Console game_______";
	string intro2="--------------GAME RACING--------------";
	string intro3="___________________\3_____________________";
	textcolor(12);
	cout<<intro<<endl;
	gotoxy(32,3);
	textcolor(11);
	cout<<intro1<<endl;
	gotoxy(32,4);
	cout<<intro2<<endl;
	gotoxy(32,5);
	cout<<intro3<<endl;
	gotoxy(0,0);
	delete_car(map,x,y);
	delete_car_computer(map,x1,y1);
	delete_car_computer_1(map,x2,y2);
	delete_car_computer_2(map,x3,y3);
	while(true)
	{
		if(life<=0)
		{
			system("cls");
			cout<<"        GAME OVER            "<<endl;
			cout<<"Total scores : "<<diem<<endl;
			cout<<"Lever : "<<diem/2500<<endl;
			cout<<"You want to play again : ";
			textcolor(60);
			cout<<"YES(Space)"<<endl;
			textcolor(7);
			while(true)
			{
				if(GetAsyncKeyState(VK_SPACE))
				{
					system("cls");
					goto cuong;
				}
			}
			return 0;
		}
		if(x1<31)x1++;
		int temp1=2+rand()%8;
		if(x2<31)x2++;
		int temp2=2+rand()%8;
		if(x3<31)x3++;
		int temp3=2+rand()%8;
		if(y1>3&&y1<28)
		{
			if(temp1%2==0) y1++;
			else y1--;
		}
		else if(y1==3)y1++;
		else if(y1==28)y1--;
		if(y2>3&&y2<28)
		{
			if(temp2%2==0) y2++;
			else y2--;
		}
		else if(y2==3)y2++;
		else if(y2==28)y2--;
		if(y3>3&&y3<28)
		{
			if(temp3%2==0) y3++;
			else y3--;
		}
		else if(y3==3)y3++;
		else if(y3==28)y3--;
		if(x1==30)
		{
			x1=0+rand()%-6;
			srand(time(0));
			y1=2+rand() %-10;
		}
		if(x2==30)
		{
			x2=0+rand()%-6;
			srand(time(0));
			y2=18+rand() %-11;
		}
		if(x3==30)
		{
			x3=0+rand()%-6;
			//srand(time(0));
			//y2=14+rand() %14;
			y3=14;
		}
		XoaManHinh();
		if(x1<=29&&y1<=29)car_computer(map,x1,y1);else x1=2,y1=10;
		if(x2<=29&&y2<=29)car_computer_1(map,x2,y2);else x2=6,y2=20;
		if(x3<=29&&y3<=29)car_computer_2(map,x3,y3); else x3=4,y3=14;
		if(x<=29&&y<=29)if(map[x][y]== '!'||map[x][y - 1] =='!'||map[x][y + 1] == '!'||map[x - 1][y - 1] =='!'||map[x + 1][y - 1] =='!'||map[x - 1][y + 1] =='!'||map[x + 1][y + 1] =='!')
	    {
		  cout<<"\a";
		  life--;
		  system("cls");
		  textcolor(7);
		  resizeConsole(650,500);
		  goto cuongpham;
	    }
		if(x<=29&&y<=29)car(map,x,y);else x=28,y=14;
		print_road(map);
		diem+=100;
		textcolor(10);
		cout<<"Life : "<<life<<endl;
		textcolor(11);
		cout<<"Lever : "<<diem/2500<<endl;
		textcolor(12);
		cout<<"Scores : "<<diem;
		if(x<=29&&y<=29)delete_car(map,x,y);
		if(x1<=29&&y1<=29)delete_car_computer(map,x1,y1);else x1=2,y1=10;
		if(x2<=29&&y2<=29)delete_car_computer_1(map,x2,y2);else x2=6,y2=20;
		if(x3<=29&&y3<=29)delete_car_computer_2(map,x3,y3);else x3=4,y3=14;
		if(GetAsyncKeyState(VK_RIGHT))
	    {
		  XoaManHinh();
		  if(y<28)y++;
		  //set_up_road(map);
		 if(x<=29&&y<=29)car(map,x,y);else x=28,y=14;
	     print_road(map);
		 diem+=100;
		 textcolor(10);
		 cout<<"Life : "<<life<<endl;
		 textcolor(11);
		 cout<<"Lever : "<<diem/2500<<endl;
		 textcolor(12);
		 cout<<"Scores : "<<diem;
		 //test(map,x,y);
	     if(x<=29&&y<=29)delete_car(map,x,y);else x=28,y=14;
		  //printf("\a");
	     }
		if(GetAsyncKeyState(VK_LEFT))
	    {
		  XoaManHinh();
		  if(y>1)y--;
		  //set_up_road(map);
		  //Sleep(0);
		  //car_computer(map,x1,y1);
		  if(x<=29&&y<=29)car(map,x,y);else x=28,y=14;
		  //test(map,x,y);
	      print_road(map);
		  diem+=100;
		  textcolor(10);
		  cout<<"Life : "<<life<<endl;
		  textcolor(11);
		  cout<<"Lever : "<<diem/2500<<endl;
		  textcolor(12);
		  cout<<"Scores : "<<diem;
	      if(x<=29&&y<=29)delete_car(map,x,y);else x=28,y=14;
	      //delete_car_computer(map,x,y);
		  //printf("\a");
		}
	}
	textcolor(7);
	system("pause");
	return 0;
}